from .models import Transaction
from rest_framework_mongoengine.serializers import DocumentSerializer


class TransactionSerializer(DocumentSerializer):
    class Meta:
        model = Transaction
