import mongoengine
import datetime


class TransactionInfo(mongoengine.EmbeddedDocument):
    """
    TransactionInfo EmbeddedDocument
    """
    user_id = mongoengine.StringField()
    track_id = mongoengine.StringField()
    created_at = mongoengine.DateTimeField(default=datetime.datetime.utcnow)


class TransactionStatus(mongoengine.EmbeddedDocument):
    """
    TransactionStatus EmbeddedDocument
    """
    at = mongoengine.DateTimeField(default=datetime.datetime.utcnow)
    STATUS = (('S', 'Success'),
              ('F', 'Fail'),
              ('IP', 'In Progress'),
              ('NV', 'Not Validated'),
              ('V', 'Validated'))
    status = mongoengine.StringField(max_length=3, choices=STATUS)


class TransactionBits(mongoengine.EmbeddedDocument):
    """
    TransactionBits EmbeddedDocument
    """
    celery_job_id = mongoengine.StringField()
    celery_status = mongoengine.StringField()
    proccessed = mongoengine.BooleanField(default=False)


class Transaction(mongoengine.Document):
    """
    TransactionInfo
    It has:
    info: For transaction main informations
    status: Last status for the transaction
    status_history: All statusses that has occured for the transaction
    bits: For storing server calculated data
    """
    info = mongoengine.EmbeddedDocumentField(TransactionInfo)
    status = mongoengine.EmbeddedDocumentField(TransactionStatus)
    status_history = mongoengine.ListField(mongoengine.EmbeddedDocumentField(TransactionStatus))
    bits = mongoengine.EmbeddedDocumentField(TransactionBits)
