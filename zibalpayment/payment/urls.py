from django.urls import path
from . import views


urlpatterns = [
    path('new_transaction/', views.NewTransactionView.as_view(), name='new_transaction'),
    path('transaction_callback/', views.TransactionCallbackView.as_view(), name='transaction_callback'),
    path('api/v1/transaction/', views.TransactionList.as_view()),
]
