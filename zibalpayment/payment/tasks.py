import requests
from celery import shared_task
from .models import Transaction, TransactionStatus
from notifier.lib.notifier import Notifier


@shared_task
def proccess_a_valid_transaction(track_id, transaction_id):
    """
    This method will call the verify API in zibal and after that it will call the notifier function to inform the user
    """
    payload = {
        'merchant': 'zibal',
        'trackId': track_id
    }
    requests.post('https://gateway.zibal.ir/v1/verify', data=payload)
    record = Transaction.objects(id=transaction_id).first()
    status = TransactionStatus()
    status.status = 'V'
    record.status = status
    record.status_history.append(status)
    record.save()

    Notifier(record.info.user_id).success_transaction_notify_user(record.info.track_id, str(record.id))
