from django.http import JsonResponse
from django.views import View
from braces.views import CsrfExemptMixin, JsonRequestResponseMixin
import requests
import json
from rest_framework_mongoengine.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .models import Transaction, TransactionInfo, TransactionStatus, TransactionBits
from .serializers import TransactionSerializer
from django_filters.rest_framework import DjangoFilterBackend
from django.core.paginator import Paginator
from .tasks import proccess_a_valid_transaction


class NewTransactionView(CsrfExemptMixin, JsonRequestResponseMixin, View):
    """
    This view is responsible for creating a new Transaction
    this is a post request
    Steps:
    1. Stores a transaction record
    2. Validates the data from request bosy
    3. Calls the zibal gateway for a transaction request
    4. Updates the transaction record
    5. Returns the url to the user
    """
    context = {}

    def post(self, request, *args, **kwargs):
        self.context['status'] = 'ok'

        record = Transaction()
        info = TransactionInfo()
        info.user_id = self.request_json.get('user_id')
        record.info = info
        status = TransactionStatus()
        status.status = 'IP'
        record.status = status
        record.status_history.append(status)
        record.bits = TransactionBits()
        record.save()

        if self.data_is_valid(self.request_json):
            payload = {
                'merchant': 'zibal',
                'amount': self.request_json.get('price'),
                'callbackUrl': 'http://localhost:8000/transaction/transaction_callback/',
                'orderId': str(record.id),
            }
            r = requests.post('https://gateway.zibal.ir/v1/request', json=payload)
            if r.json().get('result') == 100:
                self.context['paymentUrl'] = 'https://gateway.zibal.ir/start/{}'.format(r.json().get('trackId'))
        record.info.track_id = str(r.json().get('trackId'))
        record.save()
        return self.render_json_response(self.context)

    def data_is_valid(self, data):
        if 'user_id' not in data:
            self.context['error'] = 1
            self.context['message'] = 'Invelid user ID'
        elif 'order_id' not in data:
            self.context['error'] = 1
            self.context['message'] = 'Invelid order ID'
        elif 'price' not in data or type(data.get('price')) is not int:
            self.context['error'] = 1
            self.context['message'] = 'Invelid price'
        else:
            return True
        self.context['status'] = 'error'
        return False


class TransactionCallbackView(CsrfExemptMixin, JsonRequestResponseMixin, View):
    """
    This view is risponsible for validating a transaction,
    after user finishes a payment(Failed or Success), this
    url will be called with the required parameters
    This is a get request
    Steps:
    1. Validates the transaction via get pasrameters
    2. Reads the record from Db
    3. If transaction is valid -> Calles a celery job that verifies the payment
    """
    context = {}
    record = None

    def get_transaction(self, order_id):
        record = Transaction.objects(id=order_id).first()
        self.record = record

    def get(self, request, *args, **kwargs):
        self.context['status'] = 'ok'
        if self.transaction_is_valid(
                request.GET.get('trackId'),
                request.GET.get('success'),
                request.GET.get('status'),
                request.GET.get('orderId')):
            if self.record is None:
                self.get_transaction(request.GET.get('orderId'))
            else:
                self.record.reload()
            self.record.bits.proccessed = True
            status = TransactionStatus()
            status.status = 'NV'
            self.record.status_history.append(status)
            self.record.status = status
            self.record.save()
            proccess_a_valid_transaction.delay(self.record.info.track_id, str(self.record.id))
        return self.render_json_response(self.context)

    def transaction_is_valid(self, track_id, success, status, order_id):
        if self.record is None:
            self.get_transaction(order_id)
        else:
            self.record.reload()

        if success == '1' and (status == '1' or status == '2') and not self.record.bits.proccessed:
            return True
        t_status = TransactionStatus()
        t_status.status = 'F'
        self.record.status = t_status
        self.record.status_history.append(t_status)
        self.record.save()

        self.context['status'] = 'error'
        self.context['zibal_status'] = status
        return False


class TransactionList(ListCreateAPIView):
    """
    This view is a mongoengiene.DRF view
    It uses a transaction serializers and has a page parameter for pagination,
    and also have filtered results via user_id and track_id
    """
    model = Transaction
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer
    paginate_by = 10

    def get_queryset(self):
        objects = Transaction.objects()

        user_id = self.request.GET.get('user_id')
        if user_id is not None:
            objects = objects(info__user_id=user_id)

        track_id = self.request.GET.get('track_id')
        if track_id is not None:
            objects = objects(info__track_id=track_id)

        page_no = self.request.GET.get('page')
        if page_no is None:
            return objects
        paginator = Paginator(objects, self.paginate_by)
        page = paginator.page(page_no)
        return page.object_list
