from .models import Notification
from rest_framework_mongoengine.serializers import DocumentSerializer


class NotificationSerializer(DocumentSerializer):
    class Meta:
        model = Notification
