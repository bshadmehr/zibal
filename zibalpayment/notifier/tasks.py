import requests
from celery import shared_task
from .models import Notification, NotificationStatus, NotificationMedium, NotificationBits
import datetime as dt
from payment.models import Transaction


def handle_new_status(record, new_meduim, new_status):
    """
    Handles a new status for a notification and stores it
    """
    status = NotificationStatus()
    medium = NotificationMedium()
    medium.type = new_meduim
    status.medium = medium
    status.status = new_status
    record.history.append(status)
    if not record.bits:
        bits = NotificationBits()
        record.bits = bits

    next_check = dt.datetime.now() + dt.timedelta(minutes=1)
    record.bits.next_check_time = next_check
    record.save()


@shared_task
def send_SMS(phone_no, text, notif_id):
    """
    Sends SMS
    """
    record = Notification.objects(id=notif_id).first()
    handle_new_status(record, 'S', 'S')
    return "SMS - Sendded {} -> {}".format(text, phone_no)


@shared_task
def send_email(email, text, notif_id):
    """
    Sends email
    """
    record = Notification.objects(id=notif_id).first()
    handle_new_status(record, 'E', 'S')
    return "Email - Sendded {} -> {}".format(text, email)


@shared_task
def send_push_notification(token_type, token, text, notif_id):
    """
    Sends a push notification eigther for APNs or firebase user
    """
    record = Notification.objects(id=notif_id).first()
    try:
        if token_type == 'APNS':
            return "PushNotification - Sendded {} -> {} -> via {}".format(text, token, 'APNS')
        if token_type == 'firebase':
            return "PushNotification - Sendded {} -> {} -> via {}".format(text, token, 'FireBase')
        handle_new_status(record, 'PN', 'S')
    except Exception as e:
        print(e)
        handle_new_status(record, 'PN', 'F')


@shared_task
def send_telegram_bot(text, token, notif_id):
    """
    Sends a notification to stored token in user
    """
    record = Notification.objects(id=notif_id).first()
    try:
        payload = {
            'token': token,
            'message': text
        }
        requests.post('http://interview-bot.zibal.ir/notify', json=payload)
        handle_new_status(record, 'TB', 'S')
    except Exception as e:
        print(e)
        handle_new_status(record, 'TB', 'F')
    return "TelegramBot - Sendded {} -> {}".format(text, token)


def get_user():
    """
    Mocks a user data
    In real life, related service should be called
    """
    user = {}
    user['send_telegram_bot'] = True
    user['phone_no'] = '09388309605'
    user['email'] = 'bshadmehr76@gmail.com'
    user['push_token'] = 'push_token'
    user['telegram_token'] = 'U2FsdGVkX19HOM7rsYowpDhcQ13hNsynD8SJlGxaGZ8='
    return user


@shared_task
def redo_failed_tasks():
    """
    This job will be called every 1 min, using celery beat
    The main responsibility is to redo the failed notifications
    Steps:
    1. Finds all notifications with a not finished status and a valid next_check_ts
    2. Iterates over the notifications
    3. If a notification has finished all requested notifs successfully,
    it will marks it as finished
    4. Else it will calls the related celery task again
    """
    notifs = Notification.objects(bits__finished=False, bits__next_check_time__lte=dt.datetime.now())
    for n in notifs:
        all_done = True
        user = get_user()
        transaction = Transaction.objects(id=n.info.transaction).first()
        data = {}
        for r in n.requested_notifs:
            data[r.type] = False
        for h in n.history:
            data[h.medium.type] = True
        for medium in data:
            if not data[medium]:
                all_done = False
                if medium == 'S':
                    msg = "Hi, your transaction was successfull\nTransaction ID is: {}".format(transaction.id)
                    send_SMS.delay(msg, user['phone_no'], str(n.id))
                if medium == 'E':
                    msg = "Hi, your transaction was successfull\nTransaction ID is: {}".format(transaction.id)
                    send_email.delay(msg, user['email'], str(n.id))
                if medium == 'PN':
                    msg = "Hi, your transaction was successfull\nTransaction ID is: {}".format(transaction.id)
                    send_push_notification.delay(msg, user['push_token'], str(n.id))
                if medium == 'TB':
                    msg = "Hi, your transaction was successfull\nTransaction ID is: {}".format(transaction.id)
                    send_telegram_bot.delay(msg, user['telegram_token'], str(n.id))
            if n.bits.try_count is not None and n.bits.try_count >= 0:
                n.bits.try_count += 1
            else:
                n.bits.try_count = 1
            if all_done:
                n.bits.finished = True
            n.save()
    return 'Calculating'
