from notifier.tasks import send_SMS, send_email, send_push_notification, send_telegram_bot


class SuccessTransactionNotifier:
    """
    This class has the related methods for the meduims
    each method will call the desired celery task
    """
    def __init__(self):
        pass

    def SMS(self, user, transaction, notif_id):
        msg = "Hi, your transaction was successfull\nTransaction ID is: {}".format(transaction.id)
        send_SMS.delay(msg, user['phone_no'], notif_id)

    def Email(self, user, transaction, notif_id):
        msg = "Hi, your transaction was successfull\nTransaction ID is: {}".format(transaction.id)
        send_email.delay(msg, user['email'], notif_id)

    def PushNotification(self, user, transaction, notif_id):
        msg = "Hi, your transaction was successfull\nTransaction ID is: {}".format(transaction.id)
        send_push_notification.delay(msg, user['push_token'], notif_id)

    def TelegramBot(self, user, transaction, notif_id):
        msg = "Hi, your transaction was successfull\nTransaction ID is: {}".format(transaction.id)
        send_telegram_bot.delay(msg, user['telegram_token'], notif_id)
