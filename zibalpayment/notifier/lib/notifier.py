from .success_transaction_notifier import SuccessTransactionNotifier
from payment.models import Transaction
from notifier.models import Notification, NotificationInfo, NotificationBits, NotificationMedium


class Notifier:
    """
    Notifier handles every notification related proccess.
    for each situation, it has a method that desides what to do and
    how to send the notifications
    Current methods:
    success_transaction_notify_user
    """
    def __init__(self, user_id):
        self.user_id = user_id

    def success_transaction_notify_user(self, track_id, transaction_id):
        """
        This method handles a new successfull transaction
        it firsts finds or creates the notification with the transaction id
        then it gets the user from the user service
        it sends the notification based on the settings that user has
        """
        user = self.get_user()
        transaction = self.get_transaction(transaction_id)

        record = Notification.objects(info__transaction=transaction_id).first()
        if not record:
            record = Notification()
            info = NotificationInfo()
            info.transaction = transaction_id
            record.info = info
            record.save()

        requested_notifs = []

        # Here we should check what kind of notification user wants,
        # and we should act accordingle
        if 'send_sms' in user:
            SuccessTransactionNotifier().SMS(user, transaction, str(record.id))
            requested_notifs.append(NotificationMedium(type='S'))
        if 'send_email' in user:
            SuccessTransactionNotifier().Email(user, transaction, str(record.id))
            requested_notifs.append(NotificationMedium(type='E'))
        if 'send_pushnotification' in user:
            SuccessTransactionNotifier().PushNotification(user, transaction, str(record.id))
            requested_notifs.append(NotificationMedium(type='PN'))
        if 'send_telegram_bot' in user:
            SuccessTransactionNotifier().TelegramBot(user, transaction, str(record.id))
            requested_notifs.append(NotificationMedium(type='TB'))

        record.requested_notifs = requested_notifs
        record.save()

    def get_user(self):
        """
        Fakes a user
        in real life it should be connected to user service
        """
        user = {}
        user['send_telegram_bot'] = True
        user['phone_no'] = '09388309605'
        user['email'] = 'bshadmehr76@gmail.com'
        user['push_token'] = 'push_token'
        user['telegram_token'] = 'U2FsdGVkX19HOM7rsYowpDhcQ13hNsynD8SJlGxaGZ8='
        return user

    def get_transaction(self, transaction_id):
        """
        Finds the transaction
        """
        return Transaction.objects(id=transaction_id).first()
