import mongoengine
import datetime


class NotificationInfo(mongoengine.EmbeddedDocument):
    """
    NotificationInfo EmbeddedDocument
    """
    transaction = mongoengine.StringField()
    created_at = mongoengine.DateTimeField(default=datetime.datetime.utcnow)


class NotificationMedium(mongoengine.EmbeddedDocument):
    """
    NotificationMedium EmbeddedDocument
    """
    TYPE = (('S', 'SMS'),
            ('E', 'Email'),
            ('PN', 'Push Notification'),
            ('TB', 'Telegram Bot'),
            ('UMC', 'User Message Center'))
    type = mongoengine.StringField(max_length=3, choices=TYPE)


class NotificationStatus(mongoengine.EmbeddedDocument):
    """
    NotificationStatus EmbeddedDocument
    """
    at = mongoengine.DateTimeField(default=datetime.datetime.utcnow)
    medium = mongoengine.EmbeddedDocumentField(NotificationMedium)
    STATUS = (('S', 'Success'),
              ('F', 'Fail'))
    status = mongoengine.StringField(max_length=3, choices=STATUS)
    celery_job_id = mongoengine.StringField()
    celery_status = mongoengine.StringField()
    next_check = mongoengine.DateTimeField()


class NotificationBits(mongoengine.EmbeddedDocument):
    """
    NotificationBits EmbeddedDocument
    We use finished and next_check_ts for redoing failed tasks
    try_count can be used for not repeated a failed task after some try
    """
    finished = mongoengine.BooleanField(default=False)
    next_check_time = mongoengine.DateTimeField()
    try_count = mongoengine.IntField()


class Notification(mongoengine.Document):
    """
    NotificationInfo EmbeddedDocument
    A notification has a one to one relationship with a transaction
    It has:
    info: For storing notification info
    bits: for storing server calculated data for notification
    history: A history of all notifications failed or succedded
    """
    info = mongoengine.EmbeddedDocumentField(NotificationInfo)
    bits = mongoengine.EmbeddedDocumentField(NotificationBits)
    history = mongoengine.ListField(mongoengine.EmbeddedDocumentField(NotificationStatus))
    requested_notifs = mongoengine.ListField(mongoengine.EmbeddedDocumentField(NotificationMedium))
