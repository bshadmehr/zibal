from rest_framework_mongoengine.generics import ListCreateAPIView
from .models import Notification
from .serializers import NotificationSerializer
from django.core.paginator import Paginator


class NotificationList(ListCreateAPIView):
    """
    This view is a mongoengiene.DRF view
    it uses NotificationSerializer
    It uses page parameter for pagination
    You can filter the results via transaction
    """
    model = Notification
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    paginate_by = 10

    def get_queryset(self):
        objects = Notification.objects()

        user_id = self.request.GET.get('transaction')
        if user_id is not None:
            objects = objects(info__transaction=user_id)

        page_no = self.request.GET.get('page')
        if page_no is None:
            return objects
        paginator = Paginator(objects, self.paginate_by)
        page = paginator.page(page_no)
        return page.object_list
