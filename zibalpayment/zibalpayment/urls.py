from django.urls import path, include


urlpatterns = [
    path('transaction/', include('payment.urls')),
    path('notifier/', include('notifier.urls')),
]
